<?php

/**
 * @file
 * Administrative page callbacks for the google glass module.
 *
 * @ingroup google_glass
 */

/**
 * Implements hook_settings().
 */
function google_glass_admin_settings($form, &$form_state) {

  $form['google_glass_api_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Glass Client ID'),
    '#default_value' => variable_get('google_glass_api_client_id'),
    '#description' => t('Enter the Google Glass client ID key'),
    '#required' => TRUE,
  );

  $form['google_glass_api_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Glass Client secret'),
    '#default_value' => variable_get('google_glass_api_client_secret'),
    '#description' => t('Enter the Google Glass client secret'),
    '#required' => TRUE,
  );

  $form['google_glass_api_simple_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Glass API key'),
    '#default_value' => variable_get('google_glass_api_simple_key'),
    '#description' => t('Enter the Google Glass client simpley key'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

