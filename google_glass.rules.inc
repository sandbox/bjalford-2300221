<?php

/**
 * @file
 * Integration with the Rules module.
 */

/**
 * Implements hook_rules_action_info().
 */
function google_glass_rules_action_info() {
  $defaults =  array(
    'parameter' => array(
      'card' => array(
        'type' => 'text',
        'label' => t('Title of card'),
        'description' => t('Enter the title of the card.'),
      ),
    ),
    'group' => 'Google Glass',
  );
  $actions['google_glass_rules_action_send_new_card'] = $defaults + array(
    'label' => t('Send new card to Google Glass devices'),
  );
  $actions['google_glass_rules_action_update_card'] = $defaults + array(
    'label' => t('Send update to a card to Google Glass devices'),
  );
  $actions['google_glass_rules_action_delete_card'] = $defaults + array(
    'label' => t('Delete a card from Google Glass devices'),
  );
  return $actions;
}

/**
 * Send new card
 *
 * @param $card
 *   Array with card details.
 */
function google_glass_rules_action_send_new_card($card) {
  drupal_set_message('test new card');
}

/**
 * Send update to card
 *
 * @param $card
 *   Array with card details.
 */
function google_glass_rules_action_update_card($card) {
  drupal_set_message('test update to card');
}

/**
 * Send update to card
 *
 * @param $card
 *   Array with card details.
 */
function google_glass_rules_action_delete_card($card) {
  drupal_set_message('test delete card');
}
